import { createWebHistory, createRouter } from "vue-router";
import ListVue from './components/ListVue.vue'
import HomeVue from './components/HomeVue.vue'
import DetailVue from './components/DetailVue.vue'

const routes = [
  {
    path: "/",
    component: HomeVue,
  },
  {
    path: "/list",
    component: ListVue,
  },
  {
    path: "/detail/:id",
    component: DetailVue,
    // children : [
    //   {
    //     path: "author",
    //     component: Author,
    //   }
    // ]
  },
  {
    path: "/:anything(.*)",
    component: HomeVue,
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router; 