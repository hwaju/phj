const gulp = require('gulp');
const scss = require('gulp-sass')(require('sass'));
const sourcemaps = require('gulp-sourcemaps');
const dgbl = require('del-gulpsass-blank-lines');
const rename = require('gulp-rename');
const browserSync = require('browser-sync').create();
const ejs = require("gulp-ejs");
const data = require('gulp-data');
const watch = require('gulp-watch');
const fs = require('fs');
const clean = require('gulp-clean');
const path = require('path');

const paths = {
  dest: './resource/',
  src: './src/',
  html: {
    src: './src/html/**/*.html',
    inc: './src/include/**/*.ejs',
    dest: './resource/html'
  },
  scss: {
    src: './src/scss/*.scss',
    dest: './resource/css'
  },
  js: {
    src: './src/js/*.js',
    dest: './resource/js'
  },
  commonVariables: './commonVariables.json'
};

const scssOptions = {
  outputStyle: 'compressed', // nested, expanded, compressed
  linefeed: 'crlf',
  indentType: 'tab',
  indentWidth: 1,
  souceComments: true
};

const cssName = 'style.css';

gulp.task('clean', function () {
  return gulp.src(paths.dest, {read: false, allowEmpty: true})
    .pipe(clean());
});

gulp.task('scss:compile', function () {
  return gulp
    .src(paths.scss.src)
    .pipe(sourcemaps.init())
    .pipe(scss(scssOptions).on('error', scss.logError))
    .pipe(dgbl())
    .pipe(rename(cssName))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(paths.scss.dest))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('ejs:compile', function () {
  return gulp
    .src(paths.html.src)
    .pipe(data(function(file) {
      // return JSON.parse(fs.readFileSync(paths.commonVariables)); // 공통 변수를 정의한 JSON 파일
      const filePathFromSrc = path.relative(path.join(__dirname, 'src/html'), file.path);
      const depth = filePathFromSrc.split(path.sep).length + 1;
      const relativePath = '../'.repeat(depth);

      const jsonData = JSON.parse(fs.readFileSync(paths.commonVariables)); // 공통 변수를 정의한 JSON 파일
      return Object.assign({}, jsonData, { relativePath: relativePath });
    }))
    .pipe(ejs({},{}, { ext: '.html' }))
    .pipe(gulp.dest(paths.html.dest))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('js:compile', function () {
  return gulp
    .src(paths.js.src)
    .pipe(gulp.dest(paths.js.dest))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('browserSync', function () {
  return browserSync.init({
    port: 3333,
    server: {
      baseDir: './',
      index: '/resource/html/index.html'
    }
  });
});

gulp.task('watch', function () {
  watch([paths.html.src, paths.html.inc, paths.commonVariables], gulp.series('ejs:compile'))
    .on('unlink', function(filePath) {
      const filePathFromSrc = path.relative(path.resolve(paths.src + 'html'), filePath);
      const destFilePath = path.resolve(paths.html.dest, filePathFromSrc);
      fs.existsSync(destFilePath) && fs.unlinkSync(destFilePath);
    });
  watch(paths.scss.src, gulp.series('scss:compile'));
  watch(paths.js.src, gulp.series('js:compile'));
});

gulp.task('default', gulp.parallel('watch', 'browserSync', 'scss:compile', 'ejs:compile', 'js:compile'));
